/**
 * Generative Art en tu navegador
 * Archivo Final
 * @author UglyPixel
 */

// Defino los handlers para el width y height
let w, h;
// Defino el número de círculos que voy a crear y el ángulo
let circles = 12;
let angle = 360/circles;
/* Creo el array que va a contener los colores.
 * Como defino los colores usando el método color de p5.js
 * necesito llenar el array dentro de setup() o draw() */
let colors = [];
// Creo las variables que van a contener los fill
let fill1, fill2, fill3;

let img;

// Empiezo con la configuración
function setup() {
	// El primer argumento es el ancho, el segundo el alto
	createCanvas(windowWidth, windowHeight);
	// Guardo el tamaño del canvas en los handlers
	resizeVars();

	// Uso noLoop() para hacer al sketch estático
	//noLoop();
	// Y frameRate para configurar la velocidad de la animación
	frameRate(1);
	// Defino el angleMode para trabajar con grados
	angleMode(DEGREES);

	// Ahora sí lleno el array con los colores
	colors = [
		color(238, 252, 206),
		color(145, 199, 177),
		color(198, 222, 166),
		color(175, 242, 174),
		color(152, 210, 235),
		color(240, 207, 101),
		color(203, 255, 140)
	];

	// Defino clores aleatorios para los fill
	fill1 = colors[parseInt(random(0, 6))];
	fill2 = colors[parseInt(random(0, 6))];
	fill3 = colors[parseInt(random(0, 6))];
}

// Defino la animación
function draw() {
	// Uso blendMode(BLEND) y background(255) para crear el fondo
	blendMode(BLEND);
	background(255);
	// Y ahora cambio de blendMode para que los colores se fusionen
	blendMode(MULTIPLY);
	strokeWeight(2);

	// Defino el punto de origen en el centro del canvas
	translate(width/2, height/2);

	// Defino los tamaños y posiciones de los círculos
	let size1 = random(w*.1, w*.25);
	let size2 = random(w*.05, w*.25);
	let size3 = random(w*.2, w*.5);

	let pos1 = random(-w*.35, w*.15);
	let pos2 = random(-w*.25, w*.25);
	let pos3 = random(-w*.5, w*.15);

	// Creo un contador c para saber el número de círculo
	let c = 0;
	// Y q para guardar definir el modificador de tamaño
	let q = 1;
	// Arranco mi bucle que crea los círculos
	for(let a = 0; a < 360; a+=angle){
		noStroke();
		fill(fill1);
		// Si el contador es par multiplico los tamaños por 1.5
		if(c % 2 == 0){
			q = 1.5;
		}else{
			q = 1;
		}
		ellipse(cos(a)*pos1, sin(a)*pos1, size1 * q);
		// Creo una serie de círculos como líneas sin fill
		//fill(fill2);
		noFill();
		stroke(0);
		ellipse(cos(a)*pos2, sin(a)*pos2, size2 * q);
		noStroke();
		fill(fill3);
		ellipse(cos(a)*pos3, sin(a)*pos3, size3 * q);
		// Famoso lenguaje de programación (?)
		c++;
	}

	
}

// Actualizo las variables dependiendo del tamaño del canvas
function resizeVars() {
	w = width;
	h = height;

	if(w > h) {
		w = h;
	}else {
		h = w;
	}
}

// EventListener para guardar el sketch y cambiar los valores
function keyPressed() {
    if(key == 's') {
        save();
    }else if(key == 'r') {
        randomize();
    }
}

// EventListener para hacer el canvas responsive
function windowResized() {
	resizeCanvas(windowWidth, windowHeight);
	resizeVars();
  }