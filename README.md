Temas a cubrir en el workshop:

Introduccion
- A que llamamos Generative Art?
- Y por que Generative Art?
- Que es p5.js?
- Por que no usamos Processing entonces?
- Descarga/CodePen/OpenProcessing

Codigo
- Funciones: setup() y draw()
- Creacion del canvas
- Punto y linea
- Formas basicas
- fill y stroke
- Color
- Random
- Origen: Translate
- Rotacion y angulos
- Trigonometria: seno y coseno
- Animaciones
- blendMode

Link a los slides
https://slides.com/ezequielcollazo/generative-art-en-tu-navegador